const path = require('path');
const MongoStore = require('connect-mongo');
const express = require('express');
const helmet = require('helmet');
const xss = require('xss-clean');
const mongoSanitize = require('express-mongo-sanitize');
const compression = require('compression');
const cors = require('cors');
const passport = require('passport');
const httpStatus = require('http-status');
const bcrypt = require('bcryptjs');
const AdminJS = require('adminjs');
const AdminJSExpress = require('@adminjs/express');
const Upload = require('@adminjs/upload');
const config = require('./config/config');
const morgan = require('./config/morgan');
// const session = require('express-session');
const { jwtStrategy } = require('./config/passport');
const { authLimiter } = require('./middlewares/rateLimiter');
const routes = require('./routes/v1');
const { errorConverter, errorHandler } = require('./middlewares/error');
const ApiError = require('./utils/ApiError');
// const multer = require('multer');

// const storage = multer.diskStorage({
//   destination: function (req, file, cb) {
//     // console.log(file);
//     cb(null, '../public/images');
//   },
//   filename: function (req, file, cb) {
//     // console.log(file);
//     const fileFormat = file.originalname.split('.');
//     cb(null, file.fieldname + '-' + Date.now() + '.' + fileFormat[fileFormat.length - 1]);
//   }
// });
// const upload = multer({ storage: storage });

const User = require('./models/user.model');
const Blog = require('./models/blog.model');

const { CustomProvider } = require('./provider');

const provider = new CustomProvider({ bucket: path.join(__dirname, '../public') });

const app = express();

AdminJS.registerAdapter(require('@adminjs/mongoose'));

if (config.env !== 'test') {
  app.use(morgan.successHandler);
  app.use(morgan.errorHandler);
}

// RBAC functions
// const canEditCars = ({ currentAdmin, record }) => {
//   return currentAdmin && (
//     currentAdmin.role === 'admin'
//     || currentAdmin._id === record.param('ownerId')
//   )
// }
const canModifyUsers = ({ currentAdmin }) => currentAdmin && currentAdmin.role === 'admin';

// Pass all configuration settings to AdminJS
const adminJs = new AdminJS({
  resources: [
    {
      resource: Blog,
      options: {
        properties: {
          content: {
            type: 'richtext',
            components: {
              edit: AdminJS.bundle('./editor'),
            },
            isVisible: {
              list: false,
              show: true,
              edit: true,
            },
          },
          pv: {
            isVisible: {
              edit: false,
            },
          },
          createdAt: {
            isVisible: {
              edit: false,
              show: true,
              list: true,
              filter: true,
            },
          },
          updatedAt: {
            isVisible: {
              edit: false,
              show: true,
              list: true,
              filter: true,
            },
          },
        },
        actions: {
          edit: { isAccessible: canModifyUsers },
          delete: { isAccessible: canModifyUsers },
          new: { isAccessible: canModifyUsers },
        },
      },
      features: [
        Upload({
          provider,
          properties: {
            key: 'cover',
          },
          multiple: false,
        }),
      ],
    },
    //   {
    //   resource: Cars,
    //   options: {
    //     properties: {
    //       ownerId: { isVisible: { edit: false, show: true, list: true, filter: true } }
    //     },
    //     actions: {
    //       edit: { isAccessible: canEditCars },
    //       delete: { isAccessible: canEditCars },
    //       new: {
    //         before: async (request, { currentAdmin }) => {
    //           request.payload = {
    //             ...request.payload,
    //             ownerId: currentAdmin._id,
    //           }
    //           return request
    //         },
    //       }
    //     }
    //   }
    // },
    {
      resource: User,
      options: {
        properties: {
          password: { isVisible: false },
          newPassword: {
            type: 'string',
            isVisible: {
              list: false,
              edit: true,
              filter: false,
              show: false,
            },
          },
        },
        actions: {
          new: {
            before: async (request) => {
              if (request.payload.newPassword) {
                request.payload = {
                  ...request.payload,
                  password: await bcrypt.hash(request.payload.newPassword, 8),
                  newPassword: undefined,
                };
              }
              return request;
            },
            isAccessible: canModifyUsers,
          },
          edit: {
            isAccessible: canModifyUsers,
            before: async (request) => {
              if (request.payload.newPassword) {
                request.payload = {
                  ...request.payload,
                  password: await bcrypt.hash(request.payload.newPassword, 8),
                  newPassword: undefined,
                };
              }
              return request;
            },
          },
          delete: { isAccessible: canModifyUsers },
        },
      },
    },
  ],
  rootPath: '/admin',
});

// Build and use a router which will handle all AdminJS routes
const router = AdminJSExpress.buildAuthenticatedRouter(
  adminJs,
  {
    authenticate: async (email, password) => {
      const user = await User.findOne({ email });
      if (user) {
        const matched = await bcrypt.compare(password, user.password);
        if (matched) {
          return user;
        }
      }
      return false;
    },
    cookiePassword: config.jwt.secret,
  },
  null,
  {
    resave: true,
    saveUninitialized: false,
    store: MongoStore.create({
      mongoUrl: config.mongoose.url,
      mongoOptions: config.mongoose.config,
    }),
  }
);

router.post('/api/upload', (req, res) => {
  const { adminUser } = req.session;
  if (!adminUser) {
    return res.json({ success: 0 });
  }
  const { files } = req;
  const { file } = files;
  const { name } = file;
  const des = `images/${name}`;
  provider.upload(file, des);
  // fs.renameSync(tmp, des)
  return res.json({ url: `/${des}` });
});

app.use(adminJs.options.rootPath, router);

// set security HTTP headers
app.use(helmet());

// parse json request body
app.use(express.json());

// parse urlencoded request body
app.use(express.urlencoded({ extended: true }));

// sanitize request data
app.use(xss());
app.use(mongoSanitize());

// gzip compression
app.use(compression());
app.use(express.static(path.join(__dirname, '../public')));

// enable cors
app.use(cors());
app.options('*', cors());

// jwt authentication
app.use(passport.initialize());
passport.use('jwt', jwtStrategy);

// limit repeated failed requests to auth endpoints
if (config.env === 'production') {
  app.use('/v1/auth', authLimiter);
}

// v1 api routes
app.use('/v1', routes);

// send back a 404 error for any unknown api request
app.use((req, res, next) => {
  next(new ApiError(httpStatus.NOT_FOUND, 'Not found'));
});

// convert error to ApiError, if needed
app.use(errorConverter);

// handle error
app.use(errorHandler);

module.exports = app;
