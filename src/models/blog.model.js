const mongoose = require('mongoose');
const { toJSON, paginate, random } = require('./plugins');

const BlogSchema = mongoose.Schema(
  {
    title: {
      type: String,
      required: true,
      index: true,
      unique: true,
    },
    cover: {
      type: String,
    },
    content: {
      type: String,
      required: true,
    },
    pv: {
      type: Number,
      default: 1,
    },
    tags: [ String ],
  },
  {
    timestamps: true,
  }
);

// add plugin that converts mongoose to json
BlogSchema.plugin(toJSON);
BlogSchema.plugin(paginate);
BlogSchema.plugin(random);

BlogSchema.index({ createdAt: -1 });
BlogSchema.index({ tags: 1 });

/**
 * @typedef Blog
 */
const Blog = mongoose.model('Blog', BlogSchema);

module.exports = Blog;
