import { BasePropertyProps } from "adminjs"
import { useEditor, EditorContent } from '@tiptap/react'
import StarterKit from '@tiptap/starter-kit'
import Link from "@tiptap/extension-link";
import Image from "@tiptap/extension-image";
import classNames from 'classnames';
import { Button, theme, Icon, Modal, DropZone, Input } from '@adminjs/design-system';
import contentCSS from '@adminjs/design-system/src/utils/content-styles';
import { ThemeProvider, css } from 'styled-components';
import styled from "styled-components";
import { useState, createRef } from 'react';
const styles = css`
  .ProseMirror > * {
    cursor: text;
  }

  .ProseMirror {
    box-sizing: border-box;
    color: #454655;
    background: transparent;
    border: 1px solid #C0C0CA;
    font-size: 14px;
    line-height: 24px;
    font-family: 'Roboto',sans-serif;
    outline: none;
    padding-left: 8px;
    padding-right: 8px;
    padding-top: 4px;
    padding-bottom: 4px;
    margin-bottom: 16px;
    ${contentCSS};
  }
`;
const Theme = styled.div`
  ${styles};
`;
const MenuBar = ({ editor }) => {
  if (!editor) {
    return null
  }
  const [link, setLink] = useState('')
  let textInput = createRef();
  const [isVisible, setIsVisible] = useState(false)
  const [isVisible1, setIsVisible1] = useState(false)
  const modalProps = {
    /** ... **/
    onOverlayClick: () => setIsVisible(false),
    onClose: () => setIsVisible(false),
  }
  const modalProps1 = {
    /** ... **/
    onOverlayClick: () => setIsVisible1(false),
    onClose: () => setIsVisible1(false),
  }

  const addLink = (e) => {
    const link = textInput.current['value'];
    editor
      .chain()
      .focus()
      .extendMarkRange("link")
      .setLink({ href: link })
      .run();
    setIsVisible1(!isVisible1);
  }
  
  const onUpload = async (files: string | any[]) => {
    const file = files.length && files[0];
    console.log(file);
    const formData = new FormData();
    formData.append("file", file);
    const res = await fetch('/admin/api/upload', {
      method: 'POST',
      body: formData,
    });
    const json = await res.json();
    setTimeout(function () {
      editor.chain().focus().setImage({ src: json.url }).run();
    }, 500);
    setIsVisible(!isVisible);
  }
  return (
    <div
      style={{display: 'flex'}}
      id="toolbar"
      className="btn-toolbar mb-3 sticky-top"
      role="toolbar"
      aria-label="Toolbar with button groups"
    >
      <div className="btn-group mr-2" role="group" aria-label="First group">
        <Button
          size="icon"
          type="button"
          className={classNames('btn', {
            'btn-primary': editor.isActive('heading', { level: 2 }),
            'btn-secondary': !editor.isActive('heading', { level: 2 }),
          })}
          variant={editor.isActive('heading', { level: 2 })?'primary':'light'}
          onClick={(e) => {
            e.preventDefault();
            editor.chain().focus().toggleHeading({ level: 2 }).run()
          }}
        ><strong>H2</strong></Button>
        <Button
          size="icon"
          type="button"
          className={classNames('btn', {
            'btn-primary': editor.isActive('heading', { level: 3 }),
            'btn-secondary': !editor.isActive('heading', { level: 3 }),
          })}
          variant={editor.isActive('heading', { level: 3 })?'primary':'light'}
          onClick={(e) => {
            e.preventDefault();
            editor.chain().focus().toggleHeading({ level: 3 }).run()
          }}
        ><strong>H3</strong></Button>
        <Button
          size="icon"
          type="button"
          className={classNames('btn', {
            'btn-primary': editor.isActive('heading', { level: 4 }),
            'btn-secondary': !editor.isActive('heading', { level: 4 }),
          })}
          variant={editor.isActive('heading', { level: 4 })?'primary':'light'}
          onClick={(e) => {
            e.preventDefault();
            editor.chain().focus().toggleHeading({ level: 3 }).run()
          }}
        ><strong>H4</strong></Button>
        <Button
          size="icon"
          type="button"
          className={classNames('btn', {
            'btn-primary': editor.isActive('paragraph'),
            'btn-secondary': !editor.isActive('paragraph'),
          })}
          variant={editor.isActive('paragraph')?'primary':'light'}
          onClick={(e) => {
            e.preventDefault();
            editor.chain().focus().setParagraph().run()
          }}
        ><Icon icon="Paragraph"/></Button>
        <Button
          size="icon"
          type="button"
          className={classNames('btn', {
            'btn-primary': editor.isActive('bold'),
            'btn-secondary': !editor.isActive('bold'),
          })}
          variant={editor.isActive('bold')?'primary':'light'}
          onClick={(e) => {
            e.preventDefault();
            editor.chain().focus().toggleBold().run()
          }}
        ><Icon icon="TextBold"/></Button>
        <Button
          size="icon"
          type="button"
          className={classNames('btn', {
            'btn-primary': editor.isActive('italic'),
            'btn-secondary': !editor.isActive('italic'),
          })}
          variant={editor.isActive('italic')?'primary':'light'}
          onClick={(e) => {
            e.preventDefault();
            editor.chain().focus().toggleItalic().run()
          }}
        ><Icon icon="TextItalic"/></Button>
      </div>
      <div
        style={{marginLeft: '16px'}}
        role="group"
        aria-label="Second group"
      >
        <Button
          size="icon"
          type="button"
          className={classNames('btn', {
            'btn-primary': editor.isActive('orderedList'),
            'btn-secondary': !editor.isActive('orderedList'),
          })}
          variant={editor.isActive('orderedList')?'primary':'light'}
          onClick={(e) => {
            e.preventDefault();
            editor.chain().focus().toggleOrderedList().run()
          }}
        ><Icon icon="ListNumbered"/></Button>
        <Button
          size="icon"
          type="button"
          className={classNames('btn', {
            'btn-primary': editor.isActive('bulletList'),
            'btn-secondary': !editor.isActive('bulletList'),
          })}
          variant={editor.isActive('bulletList')?'primary':'light'}
          onClick={(e) => {
            e.preventDefault();
            editor.chain().focus().toggleBulletList().run()
          }}
        ><Icon icon="ListBulleted"/></Button>
        <Button
          size="icon"
          type="button"
          className={classNames('btn', {
            'btn-primary': editor.isActive('codeBlock'),
            'btn-secondary': !editor.isActive('codeBlock'),
          })}
          variant={editor.isActive('codeBlock')?'primary':'light'}
          onClick={(e) => {
            e.preventDefault();
            editor.chain().focus().toggleCodeBlock().run()
          }}
        ><Icon icon="Code"/></Button>
        <Button
          size="icon"
          type="button"
          className={classNames('btn', {
            'btn-primary': editor.isActive('blockquote'),
            'btn-secondary': !editor.isActive('blockquote'),
          })}
          variant={editor.isActive('blockquote')?'primary':'light'}
          onClick={(e) => {
            e.preventDefault();
            editor.chain().focus().toggleBlockquote().run()
          }}
        ><Icon icon="Quotes"/></Button>
        <Button
          size="icon"
          type="button"
          className={classNames('btn', {
            'btn-primary': editor.isActive('link'),
            'btn-secondary': !editor.isActive('link'),
          })}
          variant={editor.isActive('link')?'primary':'light'}
          onClick={(e) => {
            e.preventDefault();
            var obj = editor.getAttributes("link");
            
            setIsVisible1(!isVisible1);
          }}
        ><Icon icon="Link"/></Button>
        {editor.isActive('link') ? <Button
          size="icon"
          type="button"
          onClick={(e) => {
            e.preventDefault();
            editor.chain().focus().unsetLink().run()
          }}
        ><Icon icon="Unlink"/></Button> : ''}
        <Button
          size="icon"
          type="button"
          variant='light'
          onClick={(e) => {
            e.preventDefault();
            setIsVisible(!isVisible);
          }}
        ><Icon icon="Image"/></Button>
        {isVisible && <Modal label="上传图片" {...modalProps}>
          <DropZone onChange={onUpload} />
        </Modal>}
        {isVisible1 && <Modal label="添加链接" {...modalProps1}>
          <Input width="100%" placeholder="输入http/https开头的链接..." ref={textInput} />
          <Button variable='primary' onClick={addLink}>确定</Button>
        </Modal>}
      </div>
    </div>
  )
}

const Editor = (props: BasePropertyProps) => {
  const { record, property, onChange } = props;
  const value = record.params[property.path];
  const editor = useEditor({
    extensions: [
      StarterKit,
      Link.configure({ openOnClick: false }),
      Image,
    ],
    content: value ? value.toString() : 'Hello World!',
    onUpdate: ({ editor }) => {
      const html = editor.getHTML();
      onChange(property.path, html);
      // send the content to an API here
    },
  })
  const [ val, setVal ] = useState('');
  return (
    <ThemeProvider theme={theme}>
      <Theme>
        <MenuBar editor={editor} />
        <EditorContent className="editor" editor={editor} />
      </Theme>
    </ThemeProvider>
  )
}
export default Editor;