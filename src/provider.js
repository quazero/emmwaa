const fs = require('fs');
const path = require('path');
const { BaseProvider } = require('@adminjs/upload');

class CustomProvider extends BaseProvider {
  constructor(options) {
    super(options.bucket);
  }

  async upload(file, key) {
    const filePath = process.platform === 'win32' ? this.path(key) : this.path(key).slice(1); // adjusting file path according to OS
    await fs.promises.mkdir(path.dirname(filePath), { recursive: true });
    const readStream = fs.createReadStream(file.path);
    const writeStream = fs.createWriteStream(filePath);
    readStream.pipe(writeStream);
    readStream.on('end', function () {
      fs.unlinkSync(file.path);
    });
    // await fs.promises.rename(file.path, filePath);
  }

  async delete(key, bucket) {
    if (fs.existsSync(process.platform === 'win32' ? this.path(key, bucket) : this.path(key, bucket).slice(1))) {
      await fs.promises.unlink(process.platform === 'win32' ? this.path(key, bucket) : this.path(key, bucket).slice(1)); // adjusting file path according to OS
    }
  }

  // eslint-disable-next-line class-methods-use-this
  path(key, bucket) {
    // Windows doesn't requires the '/' in path, while UNIX system does
    return process.platform === 'win32'
      ? `${path.join(bucket || this.bucket, key)}`
      : `/${path.join(bucket || this.bucket, key)}`;
  }
}

exports.CustomProvider = CustomProvider;
